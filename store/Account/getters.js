import state from "./state.js";

export default {
  userMeta: (state) => state.userMeta,
  userOrders: (state) => state.userOrders
};
